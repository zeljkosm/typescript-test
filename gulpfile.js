const gulp = require('gulp');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('gulp-browserify');

 
gulp.task('compile-ts', function () {
    var tsProject = ts.createProject('tsconfig.json');
    var tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject())

    return tsResult.js
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(tsProject.options.outDir));
});

gulp.task("scripts", function(done){
    gulp.src("scripts/app.js")
    .pipe(browserify({
        insertGlobals : true
      }))
    .pipe(gulp.dest('./prod/js'), done());
});

gulp.task("default", gulp.series("compile-ts", "scripts"));

gulp.task("watch", function() {
    return gulp.watch("./src/**/*.ts", gulp.series("default"));
})