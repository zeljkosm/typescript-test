import { FudbalskiKlub } from "./classes/FudbalskiKlub";
import { Igrac } from "./classes/Igrac";
import * as _ from "lodash-uuid";

const klub = new FudbalskiKlub(1, "Partizan", "Klub iz Beograda", []);

const igrac1 = new Igrac(_.uuid(), "Petar", "Petrovic", "1996", true);
const igrac2 = new Igrac(_.uuid(), "Marko", "Markovic", "1993", true);
const igrac3 = new Igrac(_.uuid(), "Nikola", "Nikolic", "1994", false);

klub.listaIgraca = [igrac1, igrac2, igrac3];

export function prikazi(): string {
    const igraci: string[] = [];
    klub.listaIgraca.forEach(function(igrac) {
        let igracString = igrac.ime + " " + igrac.prezime + " (" + igrac.godiste;
        if (igrac.prvaPostava) {
            igracString += ", prva postava";
        }
        igracString += ")";
        igraci.push(igracString);
    });
    const igraciString = igraci.join("<br>");
    const prikaz = `
        <div>NAZIV:<br>${klub.naziv}</div>
        <div>OPIS:<br>${klub.opis}</div>
        <div>IGRACI:<br>${igraciString}</div>
    `;
    return prikaz;
}

export function dodaj(ime: string, prezime: string, godiste: string, prvaPostava: boolean): string {
    try {
        const noviIgrac = new Igrac(_.uuid(), ime, prezime, godiste, prvaPostava);
        klub.listaIgraca.push(noviIgrac);
        return "Igrac uspesno dodat";
    } catch {
        return "Problem prilikom dodavanja igraca";
    }
}

export function obrisi(ime: string, prezime: string): string {
    try {
        let index = -1;
        for (let i = 0; i < klub.listaIgraca.length; i++) {
            const igrac = klub.listaIgraca[i];
            if (igrac.ime === ime && igrac.prezime === prezime) {
                index = i;
            }
        }
        if (index > -1) {
            if (!klub.listaIgraca[index].prvaPostava) {
                klub.listaIgraca.splice(index, 1);
                return "Igrac uspesno obrisan.";
            } else {
                return "Igrac ne moze da bude obrisan posto je u prvoj postavi.";
            }
        } else {
            return "Igrac ne postoji.";
        }
    } catch {
        return "Problem prilikom brisanja igraca";
    }
}

export function izmeni(ime: string, prezime: string, godiste: string, prvaPostava: boolean): string {
    try {
        let igracPostoji = false;
        klub.listaIgraca.forEach(function(igrac) {
            if (igrac.ime === ime && igrac.prezime === prezime) {
                igrac.godiste = godiste;
                igrac.prvaPostava = prvaPostava;
                igracPostoji = true;
            }
        });
        if (igracPostoji) {
            return "Igrac uspesno izmenjen";
        } else {
            return "Igrac ne postoji";
        }
    } catch {
        return "Problem prilikom izmene igraca";
    }
}

export function izlistajIgrace() {
    return klub.listaIgraca;
}

console.log(prikazi());
