import { IFudbalskiKlub } from "../interfaces/IFudbalskiKlub";
import { Igrac } from "../classes/Igrac";

export class FudbalskiKlub implements IFudbalskiKlub {
    registarskaOznaka: number;
    naziv: string;
    opis: string;
    listaIgraca: Igrac[];

    constructor(registarskaOznaka: number, naziv: string, opis: string, listaIgraca: Igrac[]) {
        this.registarskaOznaka = registarskaOznaka;
        this.naziv = naziv;
        this.opis = opis;
        this.listaIgraca = listaIgraca;
    }
}


