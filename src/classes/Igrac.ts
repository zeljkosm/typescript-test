import { IIgrac } from "../interfaces/IIgrac";

export class Igrac implements IIgrac {
    registarskaOznaka: string;
    ime: string;
    prezime: string;
    godiste: string;
    prvaPostava: boolean;

    constructor(registarskaOznaka: string, ime: string, prezime: string, godiste: string, prvaPostava: boolean) {
        this.registarskaOznaka = registarskaOznaka;
        this.ime = ime;
        this.prezime = prezime;
        this.godiste = godiste;
        this.prvaPostava = prvaPostava;
    }
}
