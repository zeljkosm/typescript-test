export interface IIgrac {
    registarskaOznaka: string;
    ime: string;
    prezime: string;
    godiste: string;
    prvaPostava: boolean;
}
