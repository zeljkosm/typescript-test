import { Igrac } from "../classes/Igrac";

export interface IFudbalskiKlub {
    registarskaOznaka: number;
    naziv: string;
    opis: string;
    listaIgraca: Igrac[];
}
