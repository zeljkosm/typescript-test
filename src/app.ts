import * as funkcije from "./FudbalskiKlubFunkcije";
import { FudbalskiKlub } from "./classes/FudbalskiKlub";

console.log(funkcije.prikazi());

let akcija = "";
let timer: any = false;

const prikaziBtn = <HTMLElement>document.getElementById("prikaziBtn");
const dodajBtn = <HTMLElement>document.getElementById("dodajBtn");
const obrisiBtn = <HTMLElement>document.getElementById("obrisiBtn");
const izmeniBtn = <HTMLElement>document.getElementById("izmeniBtn");

const outputBody = <HTMLElement>document.getElementById("outputBody");

const forma = <HTMLFormElement>document.getElementById("forma");
const igraciSelect = <HTMLSelectElement>document.getElementById("igraciSelect");
const imeInput = <HTMLInputElement>document.getElementById("imeInput");
const prezimeInput = <HTMLInputElement>document.getElementById("prezimeInput");
const godisteInput = <HTMLInputElement>document.getElementById("godisteInput");
const postavaInput = <HTMLInputElement>document.getElementById("postavaInput");
const akcijaBtn = <HTMLInputElement>document.getElementById("akcijaBtn");

if (prikaziBtn) {
    prikaziBtn.addEventListener("click", function() {
        if (timer) {
            window.clearInterval(timer);
        }
        igraciSelect.disabled = true;
        imeInput.disabled = true;
        prezimeInput.disabled = true;
        godisteInput.disabled = true;
        postavaInput.disabled = true;
        akcijaBtn.disabled = true;
        outputBody.innerHTML = funkcije.prikazi();
    });
}

if (dodajBtn) {
    dodajBtn.addEventListener("click", function() {
        if (timer) {
            window.clearInterval(timer);
        }
        igraciSelect.disabled = true;
        outputBody.innerHTML = "Unesite sve podatke igraca.";
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = false;
        postavaInput.disabled = false;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "DODAJ";
        akcija = "dodaj";
    });
}

if (obrisiBtn) {
    obrisiBtn.addEventListener("click", function() {
        if (timer) {
            window.clearInterval(timer);
        }
        outputBody.innerHTML = "Unesite ime i prezime igraca ili ga izaberite sa liste.";
        igraciSelect.disabled = false;
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = true;
        postavaInput.disabled = true;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "OBRISI";
        akcija = "obrisi";
    });
}

if (izmeniBtn) {
    izmeniBtn.addEventListener("click", function() {
        if (timer) {
            window.clearInterval(timer);
        }
        outputBody.innerHTML = "Izaberite igraca i izmenite podatke.";
        igraciSelect.disabled = false;
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = false;
        postavaInput.disabled = false;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "IZMENI";
        akcija = "izmeni";
    });
}

if (forma) {
    forma.addEventListener("submit", function(e) {

        e.preventDefault();

        const ime: string = imeInput.value;
        const prezime: string = prezimeInput.value;
        const godiste: string = godisteInput.value;
        const prvaPostava: boolean = postavaInput.checked;

        let rezultat = "";
        if (akcija === "dodaj") {
            rezultat = funkcije.dodaj(ime, prezime, godiste, prvaPostava);
        } else if (akcija === "obrisi") {
            rezultat = funkcije.obrisi(ime, prezime);
        } else if (akcija === "izmeni") {
            rezultat = funkcije.izmeni(ime, prezime, godiste, prvaPostava);
        }
        outputBody.innerHTML = rezultat;
        imeInput.value = "";
        prezimeInput.value = "";
        godisteInput.value = "";
        postavaInput.checked = false;
        resetInputs();
        popuniSelect();
        timer = setTimeout(function() {
            outputBody.innerHTML = "Ponovite akciju ili izaberite drugu opciju iz menija.";
        }, 3000);
    });
}

igraciSelect.addEventListener("change", function() {
    const selectedIndex: string = this.value;
    if (selectedIndex) {
        const izabraniIgrac = funkcije.izlistajIgrace()[parseInt(selectedIndex, 10)];
        imeInput.value = izabraniIgrac.ime;
        prezimeInput.value = izabraniIgrac.prezime;
        godisteInput.value = izabraniIgrac.godiste;
        postavaInput.checked = izabraniIgrac.prvaPostava;
    } else {
        resetInputs();
    }
});

function resetInputs() {
    imeInput.value = "";
    prezimeInput.value = "";
    godisteInput.value = "";
    postavaInput.checked = false;
}

function popuniSelect() {
    let igraciOptions = `<option value=""></option>`;
    funkcije.izlistajIgrace().forEach(function(igrac, index) {
        igraciOptions += `<option value="${index}">${igrac.ime} ${igrac.prezime}</option>`;
    });
    if (igraciSelect) {
        igraciSelect.innerHTML = igraciOptions;
    }
}

popuniSelect();
