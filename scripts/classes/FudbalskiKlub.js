"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FudbalskiKlub = /** @class */ (function () {
    function FudbalskiKlub(registarskaOznaka, naziv, opis, listaIgraca) {
        this.registarskaOznaka = registarskaOznaka;
        this.naziv = naziv;
        this.opis = opis;
        this.listaIgraca = listaIgraca;
    }
    return FudbalskiKlub;
}());
exports.FudbalskiKlub = FudbalskiKlub;

//# sourceMappingURL=FudbalskiKlub.js.map
