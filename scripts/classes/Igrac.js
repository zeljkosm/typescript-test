"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Igrac = /** @class */ (function () {
    function Igrac(registarskaOznaka, ime, prezime, godiste, prvaPostava) {
        this.registarskaOznaka = registarskaOznaka;
        this.ime = ime;
        this.prezime = prezime;
        this.godiste = godiste;
        this.prvaPostava = prvaPostava;
    }
    return Igrac;
}());
exports.Igrac = Igrac;

//# sourceMappingURL=Igrac.js.map
