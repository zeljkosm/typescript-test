"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var funkcije = __importStar(require("./FudbalskiKlubFunkcije"));
console.log(funkcije.prikazi());
var akcija = "";
var timer;
var prikaziBtn = document.getElementById("prikaziBtn");
var dodajBtn = document.getElementById("dodajBtn");
var obrisiBtn = document.getElementById("obrisiBtn");
var izmeniBtn = document.getElementById("izmeniBtn");
var outputBody = document.getElementById("outputBody");
var forma = document.getElementById("forma");
var igraciSelect = document.getElementById("igraciSelect");
var imeInput = document.getElementById("imeInput");
var prezimeInput = document.getElementById("prezimeInput");
var godisteInput = document.getElementById("godisteInput");
var postavaInput = document.getElementById("postavaInput");
var akcijaBtn = document.getElementById("akcijaBtn");
if (prikaziBtn) {
    prikaziBtn.addEventListener("click", function () {
        if (timer) {
            window.clearInterval(timer);
        }
        igraciSelect.disabled = true;
        imeInput.disabled = true;
        prezimeInput.disabled = true;
        godisteInput.disabled = true;
        postavaInput.disabled = true;
        akcijaBtn.disabled = true;
        outputBody.innerHTML = funkcije.prikazi();
    });
}
if (dodajBtn) {
    dodajBtn.addEventListener("click", function () {
        if (timer) {
            window.clearInterval(timer);
        }
        igraciSelect.disabled = true;
        outputBody.innerHTML = "Unesite sve podatke igraca.";
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = false;
        postavaInput.disabled = false;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "DODAJ";
        akcija = "dodaj";
    });
}
if (obrisiBtn) {
    obrisiBtn.addEventListener("click", function () {
        if (timer) {
            window.clearInterval(timer);
        }
        outputBody.innerHTML = "Unesite ime i prezime igraca ili ga izaberite sa liste.";
        igraciSelect.disabled = false;
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = true;
        postavaInput.disabled = true;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "OBRISI";
        akcija = "obrisi";
    });
}
if (izmeniBtn) {
    izmeniBtn.addEventListener("click", function () {
        if (timer) {
            window.clearInterval(timer);
        }
        outputBody.innerHTML = "Izaberite igraca i izmenite podatke.";
        igraciSelect.disabled = false;
        imeInput.disabled = false;
        prezimeInput.disabled = false;
        godisteInput.disabled = false;
        postavaInput.disabled = false;
        akcijaBtn.disabled = false;
        akcijaBtn.innerHTML = "IZMENI";
        akcija = "izmeni";
    });
}
if (forma) {
    forma.addEventListener("submit", function () {
        var ime = imeInput.value;
        var prezime = prezimeInput.value;
        var godiste = godisteInput.value;
        var prvaPostava = postavaInput.checked;
        var rezultat = "";
        if (akcija === "dodaj") {
            rezultat = funkcije.dodaj(ime, prezime, godiste, prvaPostava);
        }
        else if (akcija === "obrisi") {
            rezultat = funkcije.obrisi(ime, prezime);
        }
        else if (akcija === "izmeni") {
            rezultat = funkcije.izmeni(ime, prezime, godiste, prvaPostava);
        }
        outputBody.innerHTML = rezultat;
        imeInput.value = "";
        prezimeInput.value = "";
        godisteInput.value = "";
        postavaInput.checked = false;
        resetInputs();
        popuniSelect();
        timer = setTimeout(function () {
            outputBody.innerHTML = "Ponovite akciju ili izaberite drugu opciju iz menija.";
        }, 3000);
    });
}
igraciSelect.addEventListener("change", function () {
    var selectedIndex = this.value;
    if (selectedIndex) {
        var izabraniIgrac = funkcije.izlistajIgrace()[parseInt(selectedIndex, 10)];
        imeInput.value = izabraniIgrac.ime;
        prezimeInput.value = izabraniIgrac.prezime;
        godisteInput.value = izabraniIgrac.godiste;
        postavaInput.checked = izabraniIgrac.prvaPostava;
    }
    else {
        resetInputs();
    }
});
function resetInputs() {
    imeInput.value = "";
    prezimeInput.value = "";
    godisteInput.value = "";
    postavaInput.checked = false;
}
function popuniSelect() {
    var igraciOptions = "<option value=\"\"></option>";
    funkcije.izlistajIgrace().forEach(function (igrac, index) {
        igraciOptions += "<option value=\"" + index + "\">" + igrac.ime + " " + igrac.prezime + "</option>";
    });
    if (igraciSelect) {
        igraciSelect.innerHTML = igraciOptions;
    }
}
popuniSelect();

//# sourceMappingURL=app.js.map
