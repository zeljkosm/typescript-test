"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var FudbalskiKlub_1 = require("./classes/FudbalskiKlub");
var Igrac_1 = require("./classes/Igrac");
var _ = __importStar(require("lodash-uuid"));
var klub = new FudbalskiKlub_1.FudbalskiKlub(1, "Partizan", "Klub iz Beograda", []);
var igrac1 = new Igrac_1.Igrac(_.uuid(), "Petar", "Petrovic", "1996", true);
var igrac2 = new Igrac_1.Igrac(_.uuid(), "Marko", "Markovic", "1993", true);
var igrac3 = new Igrac_1.Igrac(_.uuid(), "Nikola", "Nikolic", "1994", false);
klub.listaIgraca = [igrac1, igrac2, igrac3];
function prikazi() {
    var igraci = [];
    klub.listaIgraca.forEach(function (igrac) {
        var igracString = igrac.ime + " " + igrac.prezime + " (" + igrac.godiste;
        if (igrac.prvaPostava) {
            igracString += ", prva postava";
        }
        igracString += ")";
        igraci.push(igracString);
    });
    var igraciString = igraci.join("<br>");
    var prikaz = "\n        <div>NAZIV:<br>" + klub.naziv + "</div>\n        <div>OPIS:<br>" + klub.opis + "</div>\n        <div>IGRACI:<br>" + igraciString + "</div>\n    ";
    return prikaz;
}
exports.prikazi = prikazi;
function dodaj(ime, prezime, godiste, prvaPostava) {
    try {
        var noviIgrac = new Igrac_1.Igrac(_.uuid(), ime, prezime, godiste, prvaPostava);
        klub.listaIgraca.push(noviIgrac);
        return "Igrac uspesno dodat";
    }
    catch (_a) {
        return "Problem prilikom dodavanja igraca";
    }
}
exports.dodaj = dodaj;
function obrisi(ime, prezime) {
    try {
        var index = -1;
        for (var i = 0; i < klub.listaIgraca.length; i++) {
            var igrac = klub.listaIgraca[i];
            if (igrac.ime === ime && igrac.prezime === prezime) {
                index = i;
            }
        }
        if (index > -1) {
            if (!klub.listaIgraca[index].prvaPostava) {
                klub.listaIgraca.splice(index, 1);
                return "Igrac uspesno obrisan.";
            }
            else {
                return "Igrac ne moze da bude obrisan posto je u prvoj postavi.";
            }
        }
        else {
            return "Igrac ne postoji.";
        }
    }
    catch (_a) {
        return "Problem prilikom brisanja igraca";
    }
}
exports.obrisi = obrisi;
function izmeni(ime, prezime, godiste, prvaPostava) {
    try {
        var igracPostoji_1 = false;
        klub.listaIgraca.forEach(function (igrac) {
            if (igrac.ime === ime && igrac.prezime === prezime) {
                igrac.godiste = godiste;
                igrac.prvaPostava = prvaPostava;
                igracPostoji_1 = true;
            }
        });
        if (igracPostoji_1) {
            return "Igrac uspesno izmenjen";
        }
        else {
            return "Igrac ne postoji";
        }
    }
    catch (_a) {
        return "Problem prilikom izmene igraca";
    }
}
exports.izmeni = izmeni;
function izlistajIgrace() {
    return klub.listaIgraca;
}
exports.izlistajIgrace = izlistajIgrace;
console.log(prikazi());

//# sourceMappingURL=FudbalskiKlubFunkcije.js.map
